
import tkinter as tk

def est_premier(nombre):
    if nombre < 2:
        return False
    for i in range(2, int(nombre**0.5) + 1):
        if nombre % i == 0:
            return False
    return True

def creer_tableau(n):
    fenetre = tk.Tk()
    hauteur = int(n / 20) + 1
    canvas = tk.Canvas(fenetre, width=50*n, height=50*hauteur)
    canvas.pack()

    ligne = 0
    colonne = 0
    for i in range(1, n+1):
        x = colonne * 50
        y = ligne * 50
        canvas.create_rectangle(x, y, x+50, y+50, fill='red' if est_premier(i) else 'white')
        canvas.create_text(x+25, y+25, text=str(i), font=('Helvetica', 12))
        if (colonne + 1) % 20 == 0:
            ligne += 1
            colonne = 0
        else:
            colonne += 1

    return fenetre

n = int(input("Entrez un nombre : "))
fenetre = creer_tableau(n)
fenetre.mainloop()